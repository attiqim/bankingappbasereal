﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApp
{
    enum AccountType
    {
        CHECQUING_ACCOUNT = 1,
        SAVINGS_ACCOUNT
    }

    class Bank
    {
        private List<Account> _accountList;

        public Bank()
        {
            _accountList = new List<Account>();
        }

        public IEnumerable<Account> Accounts
        {
            get => (IEnumerable<Account>)((IEnumerable<Account>)_accountList).GetEnumerator();
        }

        public Account OpenAccount(string clientName, AccountType acctType, int acctNo)
        {
            //depending on account type create a savings account or a checking account
            Account newAccount;
            switch (acctType)
            {
                case AccountType.CHECQUING_ACCOUNT:
                    newAccount = new ChequingAccount(acctNo, clientName);
                     break;

                case AccountType.SAVINGS_ACCOUNT:
                    newAccount = new SavingsAccount(acctNo, clientName);
                    break;

                default:
                    newAccount = new Account(acctNo, clientName);
                    break;       
            }

            //add the acccount to the list
            _accountList.Add(newAccount);
            return newAccount;
        }

    }
}
